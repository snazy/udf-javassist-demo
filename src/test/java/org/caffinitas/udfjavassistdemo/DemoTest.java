/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.udfjavassistdemo;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;
import javassist.NotFoundException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.security.ProtectionDomain;
import java.security.SecureClassLoader;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("unchecked") public class DemoTest
{

    static AtomicInteger idgen = new AtomicInteger();

    @Test(invocationCount = 10)
    public void compileSingleClass() throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException
    {
        long t0 = System.currentTimeMillis();

        ClassPool classPool = ClassPool.getDefault();
        CtClass cc = classPool.makeClass("org.caffinitas.udfjavassistdemo.generated.Bundle" + idgen.incrementAndGet());
        cc.addInterface(classPool.get("org.caffinitas.udfjavassistdemo.UdfMethodInterface"));
        cc.addMethod(CtNewMethod.make(
            " public Object foobar(Object val) { " +
                "Double v = (Double) val;" +
                "double r = Math.sin( v.doubleValue()); " +
                "return Double.valueOf(r);" +
                "} "
            , cc));
        Object obj = cc.toClass().newInstance();

        long t = System.currentTimeMillis()-t0;
        System.out.println("single t = "+t);

        Assert.assertTrue(obj instanceof UdfMethodInterface);
    }

    static final String[] functions = {
        "sin", "cos", "tan", "atan", "asin", "acos", "abs", "ulp", "sqrt"
    };

    @Test(invocationCount = 10)
    public void compileBundle()
        throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException, NoSuchMethodException
    {
        long t0 = System.currentTimeMillis();

        //
        // CREATE BUNDLE math (
        //   FUNCTION double sin(value double) {
        //     return Math.sin(value);
        //   }
        //   FUNCTION float sin(value float) {
        //     return Math.sin(value);
        //   }
        //   FUNCTION double cos(value double) {
        //     return Math.cos(value);
        //   }
        //   FUNCTION float cos(value float) {
        //     return Math.cos(value);
        //   }
        //   FUNCTION double tan(value double) {
        //     return Math.tan(value);
        //   }
        //   FUNCTION float tan(value float) {
        //     return Math.tan(value);
        //   }
        // )
        //

        ClassPool classPool = ClassPool.getDefault();
        CtClass cc = classPool.makeClass("org.caffinitas.udfjavassistdemo.generated.Bundle" + idgen.incrementAndGet());

        for (String function : functions)
        {

            // function with double parameter

            cc.addMethod(CtNewMethod.make(
                " public Double "+function+"(Double val) { " +
                    "return Double.valueOf( Math."+function+"( val.doubleValue() ) ); " +
                    "} "
                , cc));

            // function with float parameter

            cc.addMethod(CtNewMethod.make(
                " public Float "+function+"(Float val) { " +
                    "return Float.valueOf( (float) Math."+function+"( val.doubleValue() ) ); " +
                    "} "
                , cc));

            // function with int parameter

            cc.addMethod(CtNewMethod.make(
                " public Integer "+function+"(Integer val) { " +
                    "return Integer.valueOf( (int) Math."+function+"( val.doubleValue() ) ); " +
                    "} "
                , cc));
        }

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        ProtectionDomain domain = null;
        Class cls = cc.toClass(loader, domain);

        long t = System.currentTimeMillis()-t0;
        System.out.println("bundle t = "+t);

        for (String function : functions)
        {
            Method m;
            m = cls.getMethod(function, Double.class);
            m = cls.getMethod(function, Float.class);
            m = cls.getMethod(function, Integer.class);
        }

    }
}
